# Camino Project

Create a mobile phone centric web registration system for online classes.

## Requirements

Docker-Compose and Docker-Engine

### How to run application

docker-compose up --build

Go to the browser and enter http://localhost:5000 for the address.
