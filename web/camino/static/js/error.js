toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-center",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
if ($('#error')) {
    if ($('#error').attr('data')) {
        toastr["error"]($('#error').attr('data'));
    }
}
if ($('#success')) {
    if ($('#success').attr('data')) {
        toastr["success"]($('#success').attr('data'));
    }
}
