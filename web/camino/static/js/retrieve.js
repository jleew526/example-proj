
    function formatPhoneNumber(s) {
        var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
        return (!m) ? "" : "(" + m[1] + ") " + m[2] + "-" + m[3];
    }
    
    function validateForm() {
        var phoneNumber = document.getElementById("phoneNumber");
        if (!(formatPhoneNumber(phoneNumber.value))) {
            toastr["error"]("You must enter a valid 10 digit US phone number.");
            return false;
        }
        return true;
    }
