from flask import Blueprint, request, render_template, redirect
from flask import current_app as app
from camino.models.registration import Registration
from camino.extensions import redis_store
from camino.tools.twilio_tools import send_message
import re
from random import randint
base_app = Blueprint("base_app", __name__)


@base_app.route('/')


@base_app.route('/register', methods=["GET", "POST"])
def register():
    if request.method == 'GET':
        return render_template('base_app/register.html')
    else:
        validatedForm = validateForm(request)
        if (type(validatedForm) is not dict ):
            return validatedForm
        random_code = ''.join([str(randint(0,9)) for x in xrange(0,5)])
        redis_store.set('V' + str(validatedForm['sid']), random_code, 300)
        try:
            send_message(app.config['TWILIO_ACCOUNT_SID'], app.config['TWILIO_AUTH_TOKEN'], validatedForm['phone'], app.config['TWILIO_PHONE_NUMBER'], "Your verification code is " + random_code)
        except:
            return render_template('base_app/register.html', error="You cannot register with numbers not registered on Twilio :(")       
        return render_template('base_app/verify.html', data_dict=validatedForm, post_url='/verify', legend='Enter your SMS verification code to finish registering your class.', buttontext='Sign up')
        
@base_app.route('/verify', methods=["POST"])
def verify():
    validatedForm = validateForm(request)
    if (type(validatedForm) is not dict ):
        return validatedForm
    if (not request.form.get('verificationCode')):
        return render_template('base_app/verify.html', data_dict=validatedForm, error="You must enter a verification code.", post_url='/verify', legend='Enter your SMS verification code to finish registering your class.', buttontext='Sign up')
    storedCode = redis_store.get('V' + str(validatedForm['sid']))
    if not storedCode:
        return render_template('base_app/register.html', error="Your verification code has expired.")
    if (storedCode != request.form.get('verificationCode')):
        return render_template('base_app/verify.html', data_dict=validatedForm, error="The verification code you entered was incorrect.", post_url='/verify', legend='Enter your SMS verification code to finish registering your class.', buttontext='Sign up')
    registration = Registration(validatedForm['sid'], validatedForm['phone'], validatedForm['classname']).save()
    redis_store.delete('V' + str(validatedForm['sid']))
    return render_template('base_app/retrieve.html', success="You have successfully registered your class!")

@base_app.route('/retrieve', methods=["GET", "POST"])
def retrieve_records():
    if request.method == 'GET':
        return render_template('base_app/retrieve.html')
    else:
        phone = None
        try:
            if (len(re.findall(r'\d+',request.form.get('phone'))[0]) != 10):
                raise ValueError()
            phone = request.form.get('phone')
        except ValueError:
            return render_template('base_app/retrieve.html', error="You must enter a 10 digit mobile number as your phone number.")
        random_code = ''.join([str(randint(0,9)) for x in xrange(0,5)])
        redis_store.set('R' + str(phone), random_code, 300)
        try:
            send_message(app.config['TWILIO_ACCOUNT_SID'], app.config['TWILIO_AUTH_TOKEN'], phone, app.config['TWILIO_PHONE_NUMBER'], "Your verification code is " + random_code)
        except:
            return render_template('base_app/retrieve.html', error="You cannot retrieve classes with numbers not registered on Twilio :(")     
        return render_template('base_app/verify.html', data_dict={"phone" :phone}, post_url='/viewRegistrations', 
               legend='Enter your SMS verification code to retrieve your registered classes.', buttontext='Retrieve')
        
        
@base_app.route('/viewRegistrations', methods=["GET", "POST"])
def view_registrations():
    if request.method == 'GET':
        return render_template('base_app/retrieve.html')
    else:
        if (not request.form.get('verificationCode')):
            return render_template('base_app/verify.html', data_dict={"phone" :request.form.get('phone')}, post_url='/viewRegistrations', 
               legend='Enter your SMS verification code to retrieve your registered classes.', buttontext='Retrieve', error="You must enter a verification code.")
        storedCode = redis_store.get('R' + request.form.get('phone'))
        if not storedCode:
            return render_template('base_app/retrieve.html', error="Your verification code has expired.")
        if (storedCode != request.form.get('verificationCode')):
            return render_template('base_app/verify.html', data_dict={"phone" :request.form.get('phone')}, post_url='/viewRegistrations', 
               legend='Enter your SMS verification code to retrieve your registered classes.', buttontext='Retrieve', error="The verification code you entered was incorrect.")
        registrations = Registration.query.filter(Registration.phone_number==request.form.get('phone')).all()
        redis_store.delete('R' + request.form.get('phone'))
        return render_template('base_app/viewRegistrations.html', data_dict=[x.get_dict() for x in registrations])
    
    
    
    
def validateForm(request):
    data_dict = dict()
    if (not request.form.get('classname')):
        return render_template('base_app/register.html', error="No class name specified.")
    data_dict['classname'] = request.form.get('classname')
    try:
        data_dict['sid'] = int(request.form.get('sid'))
    except ValueError:
        return render_template('base_app/register.html', error="You must enter a numerical value for your student ID #.")
    try:
        if (len(re.findall(r'\d+',request.form.get('phone'))[0]) != 10):
            raise ValueError()
        data_dict['phone'] = request.form.get('phone')
    except ValueError:
        return render_template('base_app/register.html', error="You must enter a 10 digit mobile number as your phone number.")
    return data_dict
