from flask import Flask, render_template
from base_app.views import base_app
from extensions import db, redis_store
from time import sleep

def configure_app(app, app_config):
    app.config.from_object(app_config)
    
def configure_blueprints(app):
    app.register_blueprint(base_app)

def configure_extensions(app):
    db.init_app(app)
    redis_store.init_app(app)
    with app.app_context():
        inited = False
        while (not inited):
            try:
                db.create_all()  
                inited = True
            except: 
                sleep(3)

def create_app(app_config="camino.configs.DefaultConfig"):
    app = Flask("Camino_app")
    app.root_path= app.root_path +"/camino"
    configure_app(app, app_config)
    configure_extensions(app)
    configure_blueprints(app)
    #configure_logging(app)
    print (app.config)
    return app  
