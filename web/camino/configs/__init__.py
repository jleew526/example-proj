class DefaultConfig(object):
    DEBUG = True
    TWILIO_ACCOUNT_SID = 'whoops'
    TWILIO_AUTH_TOKEN = 'later'
    TWILIO_PHONE_NUMBER = '+somephone'
    SQLALCHEMY_DATABASE_URI = 'mysql://flask:flask@db:3306/flask'
    REDIS_URL = "redis://:devpassword@redis:6379/0"
