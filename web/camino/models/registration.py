from camino.models import db, and_, or_
from datetime import datetime, timedelta


class Registration(db.Model):
    ID = db.Column(db.Integer, primary_key=True, index=True)
    student_id = db.Column(db.String(24), index=True)
    phone_number = db.Column(db.String(24), index=True)
    class_name = db.Column(db.String(30))

    def __init__(self, student_id, phone_number, class_name):
        self.student_id = student_id
        self.phone_number = phone_number
        self.class_name = class_name

    def get_dict(self):
        dic = {}
        dic['student_id'] = self.student_id
        dic['phone_number'] = self.phone_number
        dic['class_name'] = self.class_name
        return dic
        
    def save(self):
        db.session.add(self)
        db.session.commit()